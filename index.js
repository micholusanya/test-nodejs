import {
  connect
} from 'mongoose';
import bodyParser from 'body-parser';
import express from 'express';
import UserModel from './UserModel';
import cors from 'cors';

//use the connection string to connect to MongoDB database
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', {
  useNewUrlParser: true
})

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

//this endpoint get a get all users on the database
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

//this endpoint post a new user into the database
app.post('/users', (req, res) => {
  //this line will generate a new user based on the data inside the user model
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password
  //after generating user, there is need to save the user into the database user.save will help to store user information
  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

//this endpoint will find a user and update the user information
app.put('/users/:id', async (req, res) => {

  try {
    //find the user using the user id on the param in the database and set the request body to the new user data
    let userToEdit = await UserModel.findByIdAndUpdate({
      _id: req.params.id
    }, req.body, {
      new: true
    });
    res.send(userToEdit)

  } catch (error) {

    console.log(error)
    res.status(501).send(error)

  }
})

//this endpoint delete a user from database asynchronuous way
app.delete('/users/:id', async (req, res) => {
  try {

    //search if the user is in the database by the id
    const user = await UserModel.findById(req.params.id);
    //if the user exist
    if (user) {
      //find the user and delete from database
      const deletedUser = await UserModel.findByIdAndDelete(user)
      res.send({
        message: "User Deleted Successfuly",
        deletedUser
      });

    } else {
      //if the user does not exit, send an error of user not found
      res.status(404).send("User Not Found")
    }

  } catch (error) {
    console.log(error)
    res.status(501).send(error)

  }
})

app.listen(8080, () => console.log('Example app listening on port 8080!'))